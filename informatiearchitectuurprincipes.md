# Principes Common Ground - Informatiearchitectuurprincipes Gemeentelijk Gegevenslandschap

## Leeswijzer

Dit document beschrijft de informatiearchitectuurprincipes voor het Gemeentelijk Gegevenslanschap.

Het Gemeentelijk Gegevenlandschap is een nieuw ontwerp voor het gemeentelijk informatielandschap, waarbinnen gegevens en applicaties op een flexibele manier worden gebruikt. Naast het verbeteren van de dienstverlening aan inwoners en ondernemers door efficiënte bedrijfsvoering, zijn ook wettelijke verplichtingen (zoals voortkomend uit de AVG) en gewenste functionaliteit (bijvoorbeeld inwoners in staat stellen regie te voeren op eigen gegevens) belangrijke drijfveren voor de ontwikkeling van het Gemeentelijk Gegevenslandschap.

Deze notitie beschrijft de informatiearchitectuurprincipes bij het Gemeentelijk Gegevenslandschap: fundamentele keuzes die richting geven aan hoe er met gegevens en applicaties moet worden omgegaan.

Het Gemeentelijk Gegevenslandschap is nauw verbonden met de Common Ground-beweging, en kan gezien worden als de neerslag van Common Ground-uitganspunten in informatiearchitectuur. De realisatieprincipes voor Common Ground vormen dan ook een twee-eenheid met de informatiearchitectuurprincipes voor het Gemeentelijk Gegevenslanschap. Beide sets principes zijn belangrijk voor de totstandkoming van een nieuwe, flexibele en meer gezamenlijke gemeentelijke informatiehuishouding. De Common Ground-realisatieprincipes zijn in een separaat document beschreven.

Dit document is bestemd voor informatiemanagers, adviseurs en architecten bij gemeenten, leveranciers en andere partijen die betrokken zijn bij realisatie van de Common Ground-visie en de ontwikkeling van het Gemeentelijk Gegevenslandschap.
Het document is als volgt opgebouwd:

- Hoofdstuk 2 beschrijft de zes architectuurprincipes met een daarbij behorende implicaties

Dit document is in beheer bij VNG-Realisatie

## 1. Inleiding
   
Het Gemeentelijk Gegevenslandschap is een nieuw ontwerp voor het gemeentelijk informatielandschap. Deze notitie beschrijft de daarbij gehanteerde informatiearchitectuurprincipes; een aantal fundamentele keuzes die richting geven aan hoe er binnen het nieuwe landschap met gegevens en applicaties moet worden omgegaan. Deze principes zijn sterk verbonden met de realisatieprincipes voor de Common Ground, die in een separaat document zijn beschreven en in beschrijven op welke manier we het Gemeentelijk Gegevenslandschap in de praktijk realiseren.
De principes zijn bedoeld om ‘just enough’ richting te geven aan nieuwe ontwikkelingen. Bij het ontwerpen van nieuwe of aangepaste voorzieningen moet worden aangegeven hoe invulling wordt gegeven aan de principes, en welke overwegingen daarbij worden gemaakt. Daarbij geldt het pas-toe-of-leg-uit principe. Afwijkingen alleen zijn toegestaan als ze met goede argumenten worden onderbouwd en vastgelegd om daar in een later stadium op terug te kunnen komen. Zo wordt voorkomen dat belangrijke zaken over het hoofd worden gezien, en wordt geborgd dat activiteiten in lijn zijn met uitgangspunten van het Gemeentelijk Gegevenslandschap. VNG Realisatie ziet namens gemeenten toe op naleving van de principes en stelt deze waar nodig bij aan de hand van opgedane leerervaringen en voortschrijdend inzicht.
GEMMA als referentiearchitectuur kent ook principes die gemeenten helpen om met name hun binnengemeentelijke informatiehuishouding goed in te richten. De principes voor het Gemeentelijk Gegevenslandschap zijn in lijn met de GEMMA-principes maar zijn concreter van aard en gericht op het gezamenlijk ontwikkelen van de beoogde nieuwe informatiehuishouding.
Bij ieder principe wordt toegelicht waarom het belangrijk is en wat de implicaties van het gebruik ervan zijn. Bij de formulering van principes is de we-vorm gebruikt. Naast gemeenten worden hiermee ook de andere partijen bedoeld die bijdragen aan de realisatie van de Common Ground-visie en de ontwikkeling van het Gemeentelijk Gegevenslandschap, zoals leveranciers van software. In veel gevallen zijn de beschreven implicaties voorzien van een voorbeeld ter verduidelijking.

## 2. Informatiearchitectuurprincipes

### 2.1. COMPONENTGEBASEERD – We werken met componenten

We werken met componenten die afgebakende functionaliteit kennen en via gestandaardiseerde interfaces communiceren in plaats van met grote geïntegreerde (silo)systemen. We gebruiken hierbij een lagenmodel waarin we onderscheid maken tussen ‘interactie’, ‘processen’, ‘integratie’, ‘diensten’ en ‘gegevens’. Een hierop gebaseerd informatielandschap stelt gemeenten in staat om wendbaar en innovatief te opereren omdat wijziging of vervanging per component mogelijk wordt.

**Implicaties:**

- 1. De diensten die componenten leveren worden ontworpen en gebruikt volgens gangbare service-oriëntatie principes [1]:  
  - a. Autonomie - *"Services exercise a high level of control over their underlying runtime execution environment"*  
 Componenten zijn zelfstandig inzetbaar, schaalbaar en vervangbaar.
  - b. Losse koppeling - *"Service contracts impose low consumer coupling requirements and are themselves decoupled from their surrounding environment"*
    Componenten zijn onderling onafhankelijk en zelfstandig door te ontwikkelen en gebruiken. Wanneer één component niet beschikbaar is, blijven andere componenten functioneren.
  - c. Herbruikbaar - *"Services contain and express agnostic logic and can be positioned as reusable enterprise resources"*
  Componenten zijn vaker in te zetten door ze in verschillende omgevingen te gebruiken of door een component vaker te instantiëren binnen één omgeving.
  - d. Samenstelbaar - *"Services are effective composition participants, regardless of the size and complexity of the composition"*
  Componenten kunnen worden gecombineerd tot nieuwe componenten.
  - e. Abstractie - *"Service contracts only contain essential information and information about services is limited to what is published in service contracts"*
  Afnemers hoeven alleen de functionaliteit, niet de interne werking, van een component te kennen.
  - f. Servicecontract - *"Services within the same service inventory are in compliance with the same contract design standards"*
  Een component kent duidelijke voorwaarden en leveringsafspraken over de kwaliteit en het volume van mogelijk gebruik door afnemers.
  - g. Vindbaar - *"Services are supplemented with communicative metadata by which they can be effectively discovered and interpreted"*
  Services zijn voorzien van metadata en zijn goed vindbaar.
  - h. Toestandsloos - *"Services minimize resource consumption by deferring the management of state information when necessary"*
 Componenten bewaren geen statusinformatie om goed schaalbaar te zijn.

- 1. Componenten leveren conform de binnen het Gemeentelijk Gegevenslandschap opgestelde specificaties:
  - a. Functies (doet wat beschreven is dat hij moet doen).
  - b. Interfaces (biedt en gebruikt beschreven interfaces).
  - c. Standaarden (gebruikt verplicht gestelde standaarden)
  - d. Niet-functionele capaciteiten (bijvoorbeeld voor beveiliging en logging).
  
- 3. Componenten conformeren zich aan het Common Ground-lagenmodel, wat impliceert:
  - a. Componenten beperken zich tot functionaliteit binnen de laag waartoe ze behoren (bijvoorbeeld een frontend-applicatie beperkt zich tot presentatie en kent geen vastlegging van brongegevens). Als er goede redenen voor zijn, mag een component conform het pas-toe-of-leg-uit principe ook functionaliteit uit de onderliggende laag realiseren (bijvoorbeeld een front-end-applicatie die (ook) procesfunctionaliteit bevat omdat er daarvoor geen bruikbare component beschikbaar is).
  - b. Componenten voor interactie- en procesfunctionaliteit vragen gegevens op bij de bron via gestandaardiseerde interfaces (bijvoorbeeld een app vraagt brongegevens op via aanroep van een standaard API).
  - c. Componenten bewerken geen brongegevens anders dan bij de bron (bijvoorbeeld een app corrigeert een opgevraagd brongegeven niet in een lokale schaduwregistratie maar ondersteunt het aanpassen van het betreffende gegeven in de bronregistratie).
  - d. Mobiele componenten zoals apps richten zich op specifieke dienstverlening (bijvoorbeeld een app waarmee inwoners gemakkelijk vrije parkeerplaatsen kunnen vinden).
  
- 4. Componenten voldoen bij gebruik aan de voor gemeenten van toepassing zijnde standaarden voor informatiebeveiliging, authenticatie, autorisatie en logging (bijvoorbeeld de Baseline Informatiebeveiliging Gemeenten). Naast componentgebonden functionaliteit kan dit ook (deels) worden gerealiseerd door aanpalende voorzieningen in de omgeving waar componenten actief zijn.

### 2.2. OPEN – We zijn transparant waar mogelijk

De overheid stelt hoge eisen aan de transparantie van overheidsorganisaties. We willen transparant zijn in hoe we informatie en software gebruiken. Het delen en gebruiken van gegevens wordt slechts beperkt door wetgeving en niet door ideeën over het gebruik van gegevens. Gebruik van open standaarden en het beschikbaar stellen van gegevens als (linked) open data draagt hieraan bij, en geeft ook andere partijen dan de overheid de mogelijkheid om hierop diensten te ontwikkelen. Vanuit de maatschappij komt steeds meer vraag naar inzicht in hoe de overheid werkt en hoe beslissingen tot stand komen. Door als overheid open te zijn waar dit kan, wordt de informatiepositie van klanten versterkt, wordt publiek/private samenwerking gefaciliteerd en neemt het vertrouwen in de overheid toe.

**Implicaties:**

- 1. Indien beschikbaar gebruiken we open standaarden die publiekelijk beschikbaar zijn en waarvan de specificaties vrij van licentierechten mogen worden toegepast, gebruikt en gehanteerd. We werken daarbij conform de aanbevelingen van het Forum Standaardisatie (bijvoorbeeld gebruik van ‘OpenAPI Specification‘ als verplichte, en ‘JSON’ als aanbevolen standaard).
- 2. Gegevens worden conform de geldende bewaar- en vernietigingstermijnen uit de vigerende wet- en regelgeving behandeld. Gegevens worden waar dat vereist is gearchiveerd, vernietigd en duurzaam opgeslagen en beschikbaar gesteld.
- 3. Het delen en gebruiken van gegevens wordt slechts beperkt door wetgeving en niet door ideeën over het gebruik van gegevens.
- 4. Digitale voorzieningen voor het interactief opvragen van gegevens voldoen aan wettelijke eisen rondom digitale toegankelijkheid zodat voorzieningen bruikbaar zijn voor alle burgers
- 5. Ten aanzien van open data geldt:
  - a. Alle gegevensverzamelingen die maatschappelijk of economisch waardevol zijn en waarvoor geen beperkingen vanuit wetgeving of hun classificatie gelden worden als open data beschikbaar gesteld(bijvoorbeeld gegevens over door de gemeente verstrekte subsidies).
  - b. Bij het ontwerpen en implementeren van bronregistraties wordt rekening gehouden met beschikbaarstelling daarvan in de vorm van open data (bijvoorbeeld door gegevens geanonimiseerd te kunnen verstrekken).
  - c. Open data wordt beschikbaar gesteld conform standaarden van het Forum Standaardisatie, bij voorkeur betekenisvol (bijvoorbeeld als Linked Open Data), en geoptimaliseerd voor mobiele ntsluiting (bijvoorbeeld in JSON-formaat).
  - d. Open data wordt ook kenbaar gemaakt op de daarvoor beschikbare publicatiekanalen (bijvoorbeeld data.overheid.nl en het Nationaal Georegister).
- 1. Ten aanzien van de manier waarop wij werken geldt:
  - a. Als overheid geven we inzicht in hoe we werken en om welke redenen bepaalde, ook geautomatiseerde, keuzes worden gemaakt (bijvoorbeeld door te publiceren hoe het algoritme waarop keuzes zijn gebaseerd werkt).
  - b. Software wordt op een open manier ontworpen en ontwikkeld zodat meer mensen hier inzicht in hebben en de transparantie over hoe de systemen van de overheid werken te verhogen (bijvoorbeeld door waar dat is toegestaan software als open source openbaar toegankelijk te publiceren).
  - c. Er wordt gebruik gemaakt van gangbare platforms om gegevens en softwarecode, binnen wettelijke kaders, te kunnen delen (bijvoorbeeld gebruik van Github en Gitlab).
  - d. Samenwerking met betrokken partijen wordt vroegtijdig opgezocht (bijvoorbeeld door inwoners of leveranciers vanaf de start bij ontwikkelingen te betrekken).

### 2.3. VERTROUWD – We zorgen dat informatiebeveiliging en privacy op orde zijn

In een steeds meer informatiegedreven samenleving moeten burgers op gemeenten kunnen blijven vertrouwen. We garanderen daarvoor dat we ons houden aan wettelijke richtlijnen, nemen maatregelen om risico’s te minimaliseren, en verantwoorden naar betrokkenen wat we doen. We zorgen voor een goede beveiliging van gegevens en garanderen privacy. Om invulling te kunnen geven aan zowel verwachtingen van de burger als de wettelijke vereisten realiseren we een samenhangend geheel van technische, organisatorische, fysieke en procedurele maatregelen.

**Implicaties:**

- 1. Informatiebeveiliging en privacy worden integraal meegenomen bij het ontwerp en de inrichting van applicaties en infrastructuur (‘privacy en security by design’; we besteden er vanaf de start aandacht aan en niet pas aan het einde).
- 2. Het doel waarvoor gegevens gebruikt mogen worden is vastgelegd in wet- en regelgeving. We verwerken gegevens conform wet en regelgeving (bijvoorbeeld door controles op doelbinding en voldoen aan de beginselen van subsidiariteit en proportionaliteit voordat gegevens worden verstrekt).
- 3. Bij nieuwe voorzieningen zijn standaardconfiguratie-instellingen de meest veilige instellingen (‘security en privacy by default’; functionaliteit die niet nodig is wordt uitgeschakeld).
- 4. Als verwerker van gegevens nemen we maatregelen om risico's te beperken zoals het minimaliseren, scheiden, abstraheren of verbergen van gegevens (bijvoorbeeld niet meer gegevens verstrekken dan nodig is voor het doel).
- 5. Als verwerker van informatie laten we zien zorgvuldig om te gaan met gegevens door maatregelen zoals het informeren van betrokkenen en het transparant zijn over plaatsgevonden gegevensgebruik en verantwoording daarvan (bijvoorbeeld door inwoners actief te wijzen op hun rechten en het actief bieden van inzicht in de verwerking van hun gegevens).
- 6. Private of openbare netwerken die voor gegevensuitwisseling worden, worden passend bij het vereiste betrouwbaarheidsniveau van diensten, afdoende beveiligd, gemonitord en geanalyseerd (bijvoorbeeld door in situaties waarin partijen garanties nodig hebben over de identiteit van de andere partij gebruik te maken van geschikte certificaten).
- 7. Als gegevenshouder bewaken we de toegang tot vertrouwelijke gegevens, en maken afspraken met afnemers van gegevens over gebruik daarvan. Bij afwijking van de afspraken nemen we passende maatregelen (bijvoorbeeld door toegang tot gegevens te blokkeren of melding te maken van misbruik van gegevens bij de Autoriteit Persoonsgegevens).
- 8. Afnemers van gegevens zijn zelf verantwoordelijk voor behoorlijk gegevensgebruik. Afnemen van gegevens impliceert dus een verantwoordelijkheid bij de afnemer die zich bijvoorbeeld via een toestemmingsverklaring hiermee akkoord verklaart (bijvoorbeeld wanneer een derde partij gegevens van een gemeente ontvangt).
- 9. Bij uitwisseling van gegevens vindt authenticatie plaats op het niveau van de identiteit van de afnemende organisatie (‘gedelegeerde autorisatie’). De afnemende organisatie is verantwoordelijk om daar waar nodig gebruikers te authentiseren en te autoriseren (bijvoorbeeld door op basis van uit te voeren werkzaamheden te bepalen tot welke gegevens een gebruiker toegang krijgt).
- 10.  Een afnemende organisatie is verplicht een verwerkingenregister conform de AVG met gestandaardiseerde doelbindingsclaims te gebruiken (conform een door gemeenten gestandaardiseerd informatiemodel).
- 11. Ieder verzoek om gegevens wordt voorzien van een ‘doelbindingsclaim’ die wordt vastgelegd om mede aan de hand hiervan te kunnen vaststellen of een plaatsgevonden verwerking rechtmatig was (bijvoorbeeld bij het gebruik van persoonsgegevens).
- 12. Iedere gegevensuitwisseling wordt gelogd met als doel om een sluitende audit trail van informatietransacties te vormen (bijvoorbeeld door zowel aan de kant van de aanvrager als de leverancier van gegevens in samenhang op te vragen logginggegevens vast te leggen).
- 13. De integriteit van logbestanden wordt bewaakt (bijvoorbeeld door het gebruik van checksums waarmee is te constateren of bestanden gewijzigd zijn).
- 14. Logbestanden zijn duurzaam toegankelijk, geschikt voor audits, en voor het kunnen inzien van persoonsgebonden informatie door burgers. We sturen actief op het zorgvuldig loggen in een standaard formaat om logbestanden maximaal bruikbaar te maken (bijvoorbeeld door te zorgen dat gelogde gegevens over plaatsgevonden gebruik van persoonsgegevens via gestandaardiseerde API's opvraagbaar zijn door geautoriseerde afnemers zoals MijnOverheid of een gemeentelijk portaal).
- 15. In algemene zin geldt dat informatiebeveiligingsmaatregelen voldoen aan de voor gemeenten van toepassing zijnde strategische en tactische baseline informatiebeveiliging, de BIV-classificatie van de betrokken onderdelen, en een risicoanalyse vanuit procesperspectief.
- 16. Gegevens worden conform de geldende bewaar- en vernietigingstermijnen uit de vigerende wet- en regelgeving behandeld. Gegevens worden waard dat wordt vereist gearchiveerd, vernietigd en/of duurzaam opgeslagen en beschikbaar gesteld (bijvoorbeeld door al bij de vastlegging van gegevens maatregelen te nemen die een juiste bewaring garanderen).

### 2.4. EENMALIGE VASTLEGGING – We leggen gegevens eenmalig vast en vragen op bij de bron

We winnen gegevens eenmalig in, leggen ze eenmalig vast en gebruiken ze vervolgens, overeenkomstig het doel waarvoor ze ingewonnen zijn, meervoudig. Net zoals bij de landelijke basisregistraties is gebeurd, standaardiseren we ook andere bronbestanden. Eenmalig opgeslagen brongegevens worden alleen bewerkt bij de bron en komen beschikbaar voor processen die ze nodig hebben. Die slaan geen kopieën van die gegevens meer op maar vragen ze via gestandaardiseerde interfaces op bij de bron.

**Implicaties:**

- 1. Ten aanzien van bronregistraties [2] geldt:
  - a. Bij bronregistraties standaardiseert de bronhouder in overleg met stakeholders de semantiek, syntax en samenhang van gegevens.
  - b. Voor gemeentelijke gegevens vindt deze standaardisatie plaats onder regie van gemeenten (bijvoorbeeld bij het opstellen van nieuwe informatiemodellen).
  - c. Bronregistraties worden beschreven m.b.v. een standaard informatiemodel.
  - d. Gegevens kennen één bron (bijvoorbeeld een kernregistratie parkeerregio’s per gemeente).
  - e. Iedere gegevensbron heeft een eigenaar die verantwoordelijk is voor beschikbaarheid, integriteit, vertrouwelijkheid, volledigheid en kwaliteit (bijvoorbeeld Hoofd Openbare Ruimte voor parkeerregio gegevens).
  - f. Bij ieder gegeven is aangegeven welke kwaliteit het heeft om de bruikbaarheid te kunnen vaststellen. [3]
  - g. Bronregistraties en daarvan afgeleide diensten ondersteunen formele en materiële historie (bijvoorbeeld om te kunnen constateren vanaf welk moment een nieuw adres administratief was vastgelegd).
  - h. Bij gegevensuitwisseling maken bronhouder en gebruiker afspraken door middel van een gegevensleverings- en verwerkersovereenkomst (bijvoorbeeld over doel en gebruikstermijn).
  - i. Vermeende onjuistheden in gegevens worden door afnemers teruggemeld aan de desbetreffende bronhouder.
  - j. Gegevensbronnen maken hergebruik van basisregistraties (bijvoorbeeld door in registratie van parkeerregio’s gegevens uit de BAG te hergebruiken).
- 2. Ten aanzien van uitwisseling en beschikbaarstelling geldt:
  - a. Brongegevens worden niet binnen procesapplicaties opgeslagen (bijvoorbeeld door mutaties in brongegevens niet langer te ontvangen en kopie-gegevens binnen procesregistraties bij te werken).
  - b. Er vindt geen gegevensdistributie plaats; een proces kan worden genotificeerd over een gebeurtenis waarna de ontvangende applicatie de benodigde gegevens kan opvragen (“informatie-arm notificeren”).
  - c. Brongegevens worden, bij voorkeur realtime, opgehaald bij de bron (bijvoorbeeld door via een service gegevens op te halen op het moment dat ze nodig zijn).
  - d. Gegevensuitwisselingen zijn uniek identificeerbaar, worden gelogd en voorzien van relevante contextinformatie en metadata; dit alles binnen wettelijke kaders en mogelijke afwijking in bijzondere gevallen (bijvoorbeeld bij criminaliteitsbestrijding, terrorisme, ondermijning, fraudebestrijding).
  - e. Voor uitwisseling van gegevens wordt gebruik gemaakt van services met gestandaardiseerde API’s (bijvoorbeeld door via REST-services berichten in JSON formaat uit te wisselen).
  - f. De manier waarop gegevens worden geleverd past bij de gevoeligheid van betrokken gegevens (bijvoorbeeld vertrouwelijke gegevens alleen via veilige kanalen en netwerken, belangrijke transactiegegevens met gegarandeerde aflevering).
  - g. Gegevens en bedrijfsregels worden beschikbaar gesteld via services die beschikken over gestandaardiseerde interfaces die:
     - 1. beschreven zijn met standaard beschrijvingstalen (bijvoorbeeld het Open API Specification protocol).
     - 2. vindbaar zijn in een centrale catalogus (bijvoorbeeld op een afgesproken adres binnen een gesloten of publiek toegankelijk netwerk).
     - 3. aangeboden worden via integratievoorzieningen (bijvoorbeeld via een API-gateway).
     - 4. alleen bruikbaar zijn na afdoende authenticatie en autorisatie (bijvoorbeeld via het gebruik van certificaten).
  - h. Gegevensdiensten zijn atomair, consistent, isoleerbaar en duurzaam (bijvoorbeeld bij transacties waarbij meerdere partijen betrokken zijn).
  - i. Informatie- en gegevensdiensten verstrekken gegevens proportioneel en subsidiair.
  Gegevensleveringen worden ‘smal’ gehouden en bevatten niet meer gegevens dan wat noodzakelijk is voor het verwerkingsdoel (bijvoorbeeld door toegestane persoonsgegevens bij een verstrekking te minimaliseren en die op een voldoende veilige manier te verstrekken).
  - j. Gegevens moeten zodanig beschikbaar zijn dat interactie tijd-, plaats- en apparaatonafhankelijk kan plaatsvinden (bijvoorbeeld door 24x7 brongegevens beschikbaar te stellen die via mobiele apps zijn op te vragen).
  - k. Voor interactie-applicaties geldt dat als ophalen van brongegevens niet mogelijk is, en lokale opslag om functionele redenen vereist is, er bij gebruik van vertrouwelijke gegevens hoge beveiligingsmaatregelen nodig zijn (bijvoorbeeld bij apps die gebruikt moeten worden in omstandigheden waarin geen netwerk beschikbaar is).

### 2.5. REGIE OP GEGEVENS – We faciliteren regie op gegevens

Partijen die daartoe gerechtigd zijn, moeten regie kunnen voeren op welke gegevens aan wie, op welk moment en voor welk doel, worden verstrekt. Persoonlijk datamanagement draagt bij aan transparantie, inzage en correctie, digitale zelfbeschikking, privacy, dataminimalisatie, de kwaliteitsverbetering van gegevens en zelfredzaamheid van mensen. We geven daarom inzicht in wie welke gegevens bijhoudt en welke gegevens uitgewisseld worden. Burgers krijgen de mogelijkheid om toestemmingen t.a.v. uitwisseling van gegevens te beheren en, waar mogelijk, gegevens die op hen betrekking hebben te wijzigen.

**Implicaties:**

- 1. Ten aanzien van burgers en bedrijven als partijen met zeggenschap over hun gegevens geldt:
  - a. We stellen burgers in staat om hun AVG-conforme rechten op informatie, kopie, vergetelheid en
beperking uit te oefenen waar dit juridisch mogelijk is, bij: 
    - i. hun eigen gegevens (incl. inhoudelijke dossiers en gegevens in sectorale registraties);
    - ii. gegevens die zijn gebruikt om tot een besluit te komen en de regels die daarbij zijn gehanteerd (traceerbaarheid);
    - iii. gegevens die zijn ingezien en gebruikt (transparantiebeginsel): burgers krijgen direct online inzicht in de actuele verwerking van hun gegevens en de daarvoor gehanteerde doelbindingsclaims.
  - b. Burgers kunnen toestemming geven voor het gebruik van hun eigen gegevens bij taken waarvoor geen expliciete wettelijke grondslag bestaat (bijvoorbeeld door na een helder toegelicht verzoek met een digitale handtekening akkoord te geven voor een welomschreven gebruik van een set persoonsgegevens).
  - c. Burgers en bedrijven worden proactief geïnformeerd over zaken waarvan zij hebben aangegeven dat zij daarover geïnformeerd willen worden (bijvoorbeeld door hen via notificaties op de hoogte te stellen van relevante gegevens-gerelateerde activiteiten).
- 2. Ten aanzien van de gemeente als gegevensverwerker geldt:
  - a. Gemeenten beslissen binnen wettelijke kaders voor wie, welke gegevens beschikbaar worden gesteld (bijvoorbeeld door binnen applicaties zelf in te stellen welke gegevens voor derden beschikbaar komen).
  - b. Gemeenten bepalen hoe zij gegevens beschikbaar stellen voor gebruik door applicaties (bijvoorbeeld door te kiezen voor standaard API’s die via een gemeenschappelijke integratievoorziening door derden benaderbaar zijn).

### 2.6. STANDAARD – We standaardiseren maximaal

Gemeenten werken samen met steeds meer partijen. Afspraken en standaarden zijn nodig om effectief, efficiënt en veilig samen te werken en informatie uit te wisselen. Ze zijn ook nodig om voldoende onafhankelijk te blijven en de juiste samenwerkingspartners te kunnen kiezen. Standaardiseren is van belang binnen alle (lagen van) functionaliteit. Als gemeenten gaan we op ieder gebied waar dit meerwaarde biedt standaarden toepassen, en waar nodig ook zelf ontwikkelen.

**Implicaties:**

- 1. We volgen zoveel mogelijk standaarden, ook als ze niet verplicht zijn. (bijvoorbeeld door gebruik van wettelijk verplichte toegankelijkheidsstandaarden maar ook de door het Forum Standaardisatie aanbevolen JSON-standaard voor het uitwisselen van datastructuren).
- 2. We gebruiken waar mogelijk open standaarden conform de richtlijnen van het Forum Standaardisatie.
- 3. We ontwikkelen als gemeenten in afstemming met leveranciers nieuwe standaarden waar deze nodig maar nog niet beschikbaar zijn (bijvoorbeeld in de vorm van informatiemodellen en API-specificaties).
- 4. Landelijke catalogi, voorzieningen en bouwstenen worden gebruikt waar het verplicht is en zoveel mogelijk gebruikt waar het niet verplicht maar wel mogelijk is (bijvoorbeeld DigiD als verplichte en Samenwerkende Catalogi als niet-verplichte bouwsteen).
- 5. Gegevensmodellering, ontsluiting en integratie van gegevens zijn gestandaardiseerd (bijvoorbeeld door gebruik te maken gemeenschappelijke integratievoorzieningen).
- 6. Logging en auditing zijn gestandaardiseerd (bijvoorbeeld informatie over doelbinding en verwerking conform gemeentelijk informatiemodel).
- 7. Standaarden moeten in de praktijk eenvoudig bruikbaar zijn (bijvoorbeeld door JSON als formaat te gebruiken i.p.v. XML wanneer JSON voldoet).
- 8. Naast generiek toepasbare standaarden kennen we ook standaarden die taak- of functiespecifiek zijn (bijvoorbeeld voor gegevensuitwisseling binnen een sector).
- 9. Voor gebruik van API’s hanteren we de landelijke API- en URI-standaarden.
- 10. API’s worden in een vastgesteld formaat beschreven (bijvoorbeeld de ‘OpenApi Specification’).
- 11. Bij beschrijving van API’s worden ook authenticatiemogelijkheden en testfaciliteiten beschreven (bijvoorbeeld door aan te geven welke authenticatiemechanismen worden ondersteund).
- 12. Bij standaardisatie van gemeentelijke processen maken we gebruik van gestandaardiseerde API’s (bijvoorbeeld door bij het modelleren van processen rekening te houden met welke brongegevens via API’s zijn te gebruiken.

#### Voetnoten

[1] Bron: serviceorientation.com  
[2] De bronregistratie is de plaats waar het gegeven of document voor het eerst wordt vastgelegd (bron: NORA). Deze zijn verplicht om te gebruiken door partijen bij de uitvoering van taken. Voorbeelden zijn de basisregistraties en gemeentelijke kernregistraties.  
[3] Het Metamodel Informatiemodellen Gemeenten onderscheidt als waarden voor het metagegeven ‘Indicatie authentiek’: ‘authentiek’, ‘basisgegeven’, ‘landelijk kerngegeven’, ‘gemeentelijk kerngegeven’, ‘overig’.  
