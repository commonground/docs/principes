# Principes

Dit repository bevat de Principes Common Ground (Informatiearchitectuurprincipes an Realisatieprincipes)

- Informatiearchitectuurprincipes - [Markdown](informatiearchitectuurprincipes.md) - [Open PDF](informatiearchitectuurprincipes.pdf)   
- Realisatieprincipes - [Markdown](realisatieprincipes.md) - [PDF](realisatieprincipes.pdf)